var fs=require("fs");
var path=require("path");
function func(PathToFile,data)
{
    return new Promise((resolve,reject)=>
    {
        fs.writeFile(PathToFile,data,err=>
        {
            if(err)
            reject("Error writing content in file");
            else
            resolve("Content written to file successfully");
        });
    });
}
module.exports=func;
