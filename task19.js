var fs=require("fs");
var path=require("path");
var destpath=require("./task04");
function func(source,destpath)
{
    return new Promise((resolve,reject)=>
    {
        fs.access(destpath,err=>
        {
            if(!err)
            reject("cannot copy data : File exists");
            else
            {
                fs.copyFile(source,destpath,err=>
                {
                    if(!err)
                    resolve("Content copied successfully");
                });
            }

        });
    });
}
module.exports=func;
