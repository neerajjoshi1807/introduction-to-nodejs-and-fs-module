var fs=require("fs");
var path=require("path");
function func(PathToFile)
{
    var countDir=0;
    var countFile=0;
    return new Promise((resolve,reject)=>
    {
        fs.readdir(PathToFile,(err,data)=>
        {
            if(err)
            reject("Error occured while reading directory");
            else
            {
                for(var i=0;i<data.length;i++)
                {
                    if(data[i].includes("."))
                    countFile+=1;
                    else
                    countDir+=1;
                }
                var obj=
                {
                    countDir:countDir,
                    countFile:countFile,
                    files:data
                }
                resolve(obj);
            }
        });
    });
}
module.exports=func;
