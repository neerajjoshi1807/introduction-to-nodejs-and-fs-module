var path=require("path");
var fs=require("fs");
function func(PathToFile)
{
    return new Promise((resolve,reject)=>
    {
        fs.readdir(PathToFile,(err,data)=>
        {
            var countJs=0;
            var countTxt=0;
            var count=0;
            if(err)
            reject("Error occured while reading directory");
            else
            {
                for(var i=0;i<data.length;i++)
                {
                    if(path.extname(data[i])==".js")
                    countJs+=1;
                    else if(path.extname(data[i])==".txt")
                    countTxt+=1;
                    else
                    count+=1;
                }
                var obj=
                {
                    countJs:countJs,
                    countTxt:countTxt,
                    count:count,
                    files:data
                }
                resolve(obj);
            }
        });

    });
}
module.exports=func;
