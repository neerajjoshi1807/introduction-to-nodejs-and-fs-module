var fs=require("fs");
var path=require("path");
function func(PathToFile)
{
    return new Promise((resolve,reject)=>
    {
    fs.readFile(PathToFile,"utf8",(err,data)=>
    {
        if(err)
        {
            reject("Error reading file");
        }
        else
        {
            var countvar=data.split(/let|const|var/).length-1;
            var countfunc=data.split(/function|=>/).length-1;
            resolve({functionCount:countfunc,
            variableCount:countvar})
        }
    });
    });
}
module.exports=func;
